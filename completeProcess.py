import subprocess
import json
import sys
import os

process = subprocess.Popen(['python', 'unigram.py'], stdout = sys.stdout, stderr = sys.stderr)
out, err = process.communicate()

if err is not None:
	print "Problem encountered while executing unigram.py"
	quit ()

process = subprocess.Popen(['python', 'bigram.py'], stdout = sys.stdout, stderr = sys.stderr)
out, err = process.communicate()

if err is not None:
	print "Problem encountered while executing bigram.py"
	quit ()

process = subprocess.Popen(['python', 'backoff_weight.py'], stdout = sys.stdout, stderr = sys.stderr)
out, err = process.communicate()

if err is not None:
	print "Problem encountered while executing backoff_weight.py"
	quit ()
	
process = subprocess.Popen(['python', 'perplexity.py'], stdout = sys.stdout, stderr = sys.stderr)
out, err = process.communicate()

if err is not None:
	print "Problem encountered while executing perplexity.py"
	quit ()