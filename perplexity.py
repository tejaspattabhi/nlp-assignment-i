import os
import sys
import json
import math
import re
from collections import Counter
from itertools import tee, islice, izip

list = re.findall ("<\w+>.*</\w+>", open ('testData.txt').read())
words = []
for sentence in list:
	words.extend (sentence.split (' '))

try:
	words = filter (lambda a: a != "", words)
	words = filter (lambda a: a != "\t", words)
except:
	"Do Nothing!"
	
wCount = len (words)
print "Number of words in test file:", wCount
tuples = Counter (izip (words, islice (words, 1, None)))

def katzProbability (wOne, wTwo, uniJson, alJson):
	return (math.log (alJson[wOne] * uniJson[wTwo]["probability"], 2))

with open ("unigram.json", "r") as file:
	uniJson = json.load (file)
	file.close ()
	
with open ("bigram.json", "r") as file:
	biJson = json.load (file)
	file.close ()

with open ("alpha.json", "r") as file:
	alJson = json.load (file)
	file.close ()

logList = []
# print tuples
for (wOne, wTwo) in tuples:
	# print wOne + " " + wTwo 
	if wOne + " " + wTwo in biJson:
		# print "present"
		logList.append (math.log (biJson[wOne + " " + wTwo]["probability"], 2))
	else:
		# print "not present"
		logList.append (katzProbability (wOne, wTwo, uniJson, alJson))

exponent = -1 * (float (1) / wCount) * sum (logList)
print "Exponent: ", exponent
print "Perplexity of this whole shit is: ", (2 ** exponent)